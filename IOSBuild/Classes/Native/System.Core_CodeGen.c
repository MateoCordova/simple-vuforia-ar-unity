﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000008 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000A TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x0000000D System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000012 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000015 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000016 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000017 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001C TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000001E System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000001F System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000020 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000021 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000022 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000024 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000025 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000026 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000028 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000033 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000037 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000038 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000042 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000043 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000046 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::.ctor(System.Int32)
// 0x00000047 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.IDisposable.Dispose()
// 0x00000048 System.Boolean System.Linq.Enumerable_<SelectIterator>d__5`2::MoveNext()
// 0x00000049 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::<>m__Finally1()
// 0x0000004A TResult System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000004B System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.Reset()
// 0x0000004C System.Object System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.get_Current()
// 0x0000004D System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000004E System.Collections.IEnumerator System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004F System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000050 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000051 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000052 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000053 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x00000054 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x00000055 System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x00000056 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x00000057 TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000058 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x00000059 System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x0000005A System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000005B System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005C System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x0000005D System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x0000005E System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x0000005F System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000060 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000061 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000062 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000063 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000064 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000065 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x00000066 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000067 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000068 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000069 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000006A System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000006B System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000006C System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000006D System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x0000006E System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000006F System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000070 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000071 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000072 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000073 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000074 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000075 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000076 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000077 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000078 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000079 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000007A System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000007B System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000007C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000007D System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000007E System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x0000007F System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000082 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000083 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000084 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000085 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000086 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000087 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000089 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000008A T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000008B System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000008C System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[140] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[140] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[45] = 
{
	{ 0x02000004, { 77, 4 } },
	{ 0x02000005, { 81, 9 } },
	{ 0x02000006, { 92, 7 } },
	{ 0x02000007, { 101, 10 } },
	{ 0x02000008, { 113, 11 } },
	{ 0x02000009, { 127, 9 } },
	{ 0x0200000A, { 139, 12 } },
	{ 0x0200000B, { 154, 9 } },
	{ 0x0200000C, { 163, 1 } },
	{ 0x0200000D, { 164, 2 } },
	{ 0x0200000E, { 166, 6 } },
	{ 0x0200000F, { 172, 6 } },
	{ 0x02000010, { 178, 2 } },
	{ 0x02000011, { 180, 4 } },
	{ 0x02000012, { 184, 34 } },
	{ 0x02000014, { 218, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 10 } },
	{ 0x06000006, { 20, 1 } },
	{ 0x06000007, { 21, 2 } },
	{ 0x06000008, { 23, 5 } },
	{ 0x06000009, { 28, 5 } },
	{ 0x0600000A, { 33, 3 } },
	{ 0x0600000B, { 36, 2 } },
	{ 0x0600000C, { 38, 1 } },
	{ 0x0600000D, { 39, 7 } },
	{ 0x0600000E, { 46, 1 } },
	{ 0x0600000F, { 47, 2 } },
	{ 0x06000010, { 49, 2 } },
	{ 0x06000011, { 51, 2 } },
	{ 0x06000012, { 53, 4 } },
	{ 0x06000013, { 57, 4 } },
	{ 0x06000014, { 61, 3 } },
	{ 0x06000015, { 64, 3 } },
	{ 0x06000016, { 67, 1 } },
	{ 0x06000017, { 68, 1 } },
	{ 0x06000018, { 69, 3 } },
	{ 0x06000019, { 72, 3 } },
	{ 0x0600001A, { 75, 2 } },
	{ 0x0600002A, { 90, 2 } },
	{ 0x0600002F, { 99, 2 } },
	{ 0x06000034, { 111, 2 } },
	{ 0x0600003A, { 124, 3 } },
	{ 0x0600003F, { 136, 3 } },
	{ 0x06000044, { 151, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[220] = 
{
	{ (Il2CppRGCTXDataType)2, 16645 },
	{ (Il2CppRGCTXDataType)3, 12354 },
	{ (Il2CppRGCTXDataType)2, 16646 },
	{ (Il2CppRGCTXDataType)2, 16647 },
	{ (Il2CppRGCTXDataType)3, 12355 },
	{ (Il2CppRGCTXDataType)2, 16648 },
	{ (Il2CppRGCTXDataType)2, 16649 },
	{ (Il2CppRGCTXDataType)3, 12356 },
	{ (Il2CppRGCTXDataType)2, 16650 },
	{ (Il2CppRGCTXDataType)3, 12357 },
	{ (Il2CppRGCTXDataType)2, 16651 },
	{ (Il2CppRGCTXDataType)3, 12358 },
	{ (Il2CppRGCTXDataType)2, 16652 },
	{ (Il2CppRGCTXDataType)2, 16653 },
	{ (Il2CppRGCTXDataType)3, 12359 },
	{ (Il2CppRGCTXDataType)2, 16654 },
	{ (Il2CppRGCTXDataType)2, 16655 },
	{ (Il2CppRGCTXDataType)3, 12360 },
	{ (Il2CppRGCTXDataType)2, 16656 },
	{ (Il2CppRGCTXDataType)3, 12361 },
	{ (Il2CppRGCTXDataType)3, 12362 },
	{ (Il2CppRGCTXDataType)2, 16657 },
	{ (Il2CppRGCTXDataType)3, 12363 },
	{ (Il2CppRGCTXDataType)2, 16658 },
	{ (Il2CppRGCTXDataType)3, 12364 },
	{ (Il2CppRGCTXDataType)3, 12365 },
	{ (Il2CppRGCTXDataType)2, 12901 },
	{ (Il2CppRGCTXDataType)3, 12366 },
	{ (Il2CppRGCTXDataType)2, 16659 },
	{ (Il2CppRGCTXDataType)3, 12367 },
	{ (Il2CppRGCTXDataType)3, 12368 },
	{ (Il2CppRGCTXDataType)2, 12908 },
	{ (Il2CppRGCTXDataType)3, 12369 },
	{ (Il2CppRGCTXDataType)2, 16660 },
	{ (Il2CppRGCTXDataType)3, 12370 },
	{ (Il2CppRGCTXDataType)3, 12371 },
	{ (Il2CppRGCTXDataType)2, 12914 },
	{ (Il2CppRGCTXDataType)3, 12372 },
	{ (Il2CppRGCTXDataType)3, 12373 },
	{ (Il2CppRGCTXDataType)2, 12929 },
	{ (Il2CppRGCTXDataType)3, 12374 },
	{ (Il2CppRGCTXDataType)2, 12922 },
	{ (Il2CppRGCTXDataType)2, 16661 },
	{ (Il2CppRGCTXDataType)3, 12375 },
	{ (Il2CppRGCTXDataType)3, 12376 },
	{ (Il2CppRGCTXDataType)3, 12377 },
	{ (Il2CppRGCTXDataType)3, 12378 },
	{ (Il2CppRGCTXDataType)2, 16662 },
	{ (Il2CppRGCTXDataType)3, 12379 },
	{ (Il2CppRGCTXDataType)2, 12934 },
	{ (Il2CppRGCTXDataType)3, 12380 },
	{ (Il2CppRGCTXDataType)2, 16663 },
	{ (Il2CppRGCTXDataType)3, 12381 },
	{ (Il2CppRGCTXDataType)2, 16664 },
	{ (Il2CppRGCTXDataType)2, 16665 },
	{ (Il2CppRGCTXDataType)2, 12938 },
	{ (Il2CppRGCTXDataType)2, 16666 },
	{ (Il2CppRGCTXDataType)2, 16667 },
	{ (Il2CppRGCTXDataType)2, 16668 },
	{ (Il2CppRGCTXDataType)2, 12940 },
	{ (Il2CppRGCTXDataType)2, 16669 },
	{ (Il2CppRGCTXDataType)2, 12942 },
	{ (Il2CppRGCTXDataType)2, 16670 },
	{ (Il2CppRGCTXDataType)3, 12382 },
	{ (Il2CppRGCTXDataType)2, 12945 },
	{ (Il2CppRGCTXDataType)2, 16671 },
	{ (Il2CppRGCTXDataType)3, 12383 },
	{ (Il2CppRGCTXDataType)2, 16672 },
	{ (Il2CppRGCTXDataType)2, 12950 },
	{ (Il2CppRGCTXDataType)2, 12952 },
	{ (Il2CppRGCTXDataType)2, 16673 },
	{ (Il2CppRGCTXDataType)3, 12384 },
	{ (Il2CppRGCTXDataType)2, 12955 },
	{ (Il2CppRGCTXDataType)2, 16674 },
	{ (Il2CppRGCTXDataType)3, 12385 },
	{ (Il2CppRGCTXDataType)2, 16675 },
	{ (Il2CppRGCTXDataType)2, 12958 },
	{ (Il2CppRGCTXDataType)3, 12386 },
	{ (Il2CppRGCTXDataType)3, 12387 },
	{ (Il2CppRGCTXDataType)2, 12962 },
	{ (Il2CppRGCTXDataType)3, 12388 },
	{ (Il2CppRGCTXDataType)3, 12389 },
	{ (Il2CppRGCTXDataType)2, 12974 },
	{ (Il2CppRGCTXDataType)2, 16676 },
	{ (Il2CppRGCTXDataType)3, 12390 },
	{ (Il2CppRGCTXDataType)3, 12391 },
	{ (Il2CppRGCTXDataType)2, 12976 },
	{ (Il2CppRGCTXDataType)2, 16546 },
	{ (Il2CppRGCTXDataType)3, 12392 },
	{ (Il2CppRGCTXDataType)3, 12393 },
	{ (Il2CppRGCTXDataType)2, 16677 },
	{ (Il2CppRGCTXDataType)3, 12394 },
	{ (Il2CppRGCTXDataType)3, 12395 },
	{ (Il2CppRGCTXDataType)2, 12986 },
	{ (Il2CppRGCTXDataType)2, 16678 },
	{ (Il2CppRGCTXDataType)3, 12396 },
	{ (Il2CppRGCTXDataType)3, 12397 },
	{ (Il2CppRGCTXDataType)3, 11943 },
	{ (Il2CppRGCTXDataType)3, 12398 },
	{ (Il2CppRGCTXDataType)2, 16679 },
	{ (Il2CppRGCTXDataType)3, 12399 },
	{ (Il2CppRGCTXDataType)3, 12400 },
	{ (Il2CppRGCTXDataType)2, 12998 },
	{ (Il2CppRGCTXDataType)2, 16680 },
	{ (Il2CppRGCTXDataType)3, 12401 },
	{ (Il2CppRGCTXDataType)3, 12402 },
	{ (Il2CppRGCTXDataType)3, 12403 },
	{ (Il2CppRGCTXDataType)3, 12404 },
	{ (Il2CppRGCTXDataType)3, 12405 },
	{ (Il2CppRGCTXDataType)3, 11949 },
	{ (Il2CppRGCTXDataType)3, 12406 },
	{ (Il2CppRGCTXDataType)2, 16681 },
	{ (Il2CppRGCTXDataType)3, 12407 },
	{ (Il2CppRGCTXDataType)3, 12408 },
	{ (Il2CppRGCTXDataType)2, 13011 },
	{ (Il2CppRGCTXDataType)2, 16682 },
	{ (Il2CppRGCTXDataType)3, 12409 },
	{ (Il2CppRGCTXDataType)3, 12410 },
	{ (Il2CppRGCTXDataType)2, 13013 },
	{ (Il2CppRGCTXDataType)2, 16683 },
	{ (Il2CppRGCTXDataType)3, 12411 },
	{ (Il2CppRGCTXDataType)3, 12412 },
	{ (Il2CppRGCTXDataType)2, 16684 },
	{ (Il2CppRGCTXDataType)3, 12413 },
	{ (Il2CppRGCTXDataType)3, 12414 },
	{ (Il2CppRGCTXDataType)2, 16685 },
	{ (Il2CppRGCTXDataType)3, 12415 },
	{ (Il2CppRGCTXDataType)3, 12416 },
	{ (Il2CppRGCTXDataType)2, 13028 },
	{ (Il2CppRGCTXDataType)2, 16686 },
	{ (Il2CppRGCTXDataType)3, 12417 },
	{ (Il2CppRGCTXDataType)3, 12418 },
	{ (Il2CppRGCTXDataType)3, 12419 },
	{ (Il2CppRGCTXDataType)3, 11960 },
	{ (Il2CppRGCTXDataType)2, 16687 },
	{ (Il2CppRGCTXDataType)3, 12420 },
	{ (Il2CppRGCTXDataType)3, 12421 },
	{ (Il2CppRGCTXDataType)2, 16688 },
	{ (Il2CppRGCTXDataType)3, 12422 },
	{ (Il2CppRGCTXDataType)3, 12423 },
	{ (Il2CppRGCTXDataType)2, 13044 },
	{ (Il2CppRGCTXDataType)2, 16689 },
	{ (Il2CppRGCTXDataType)3, 12424 },
	{ (Il2CppRGCTXDataType)3, 12425 },
	{ (Il2CppRGCTXDataType)3, 12426 },
	{ (Il2CppRGCTXDataType)3, 12427 },
	{ (Il2CppRGCTXDataType)3, 12428 },
	{ (Il2CppRGCTXDataType)3, 12429 },
	{ (Il2CppRGCTXDataType)3, 11966 },
	{ (Il2CppRGCTXDataType)2, 16690 },
	{ (Il2CppRGCTXDataType)3, 12430 },
	{ (Il2CppRGCTXDataType)3, 12431 },
	{ (Il2CppRGCTXDataType)2, 16691 },
	{ (Il2CppRGCTXDataType)3, 12432 },
	{ (Il2CppRGCTXDataType)3, 12433 },
	{ (Il2CppRGCTXDataType)2, 16692 },
	{ (Il2CppRGCTXDataType)2, 16693 },
	{ (Il2CppRGCTXDataType)3, 12434 },
	{ (Il2CppRGCTXDataType)3, 12435 },
	{ (Il2CppRGCTXDataType)2, 13061 },
	{ (Il2CppRGCTXDataType)2, 16694 },
	{ (Il2CppRGCTXDataType)3, 12436 },
	{ (Il2CppRGCTXDataType)3, 12437 },
	{ (Il2CppRGCTXDataType)3, 12438 },
	{ (Il2CppRGCTXDataType)3, 12439 },
	{ (Il2CppRGCTXDataType)3, 12440 },
	{ (Il2CppRGCTXDataType)3, 12441 },
	{ (Il2CppRGCTXDataType)2, 13085 },
	{ (Il2CppRGCTXDataType)3, 12442 },
	{ (Il2CppRGCTXDataType)2, 16695 },
	{ (Il2CppRGCTXDataType)3, 12443 },
	{ (Il2CppRGCTXDataType)3, 12444 },
	{ (Il2CppRGCTXDataType)3, 12445 },
	{ (Il2CppRGCTXDataType)2, 13093 },
	{ (Il2CppRGCTXDataType)3, 12446 },
	{ (Il2CppRGCTXDataType)2, 16696 },
	{ (Il2CppRGCTXDataType)3, 12447 },
	{ (Il2CppRGCTXDataType)3, 12448 },
	{ (Il2CppRGCTXDataType)2, 16697 },
	{ (Il2CppRGCTXDataType)2, 16698 },
	{ (Il2CppRGCTXDataType)2, 16699 },
	{ (Il2CppRGCTXDataType)2, 13106 },
	{ (Il2CppRGCTXDataType)2, 13104 },
	{ (Il2CppRGCTXDataType)2, 16700 },
	{ (Il2CppRGCTXDataType)3, 12449 },
	{ (Il2CppRGCTXDataType)2, 16701 },
	{ (Il2CppRGCTXDataType)3, 12450 },
	{ (Il2CppRGCTXDataType)3, 12451 },
	{ (Il2CppRGCTXDataType)2, 13113 },
	{ (Il2CppRGCTXDataType)3, 12452 },
	{ (Il2CppRGCTXDataType)2, 13113 },
	{ (Il2CppRGCTXDataType)3, 12453 },
	{ (Il2CppRGCTXDataType)2, 13130 },
	{ (Il2CppRGCTXDataType)3, 12454 },
	{ (Il2CppRGCTXDataType)3, 12455 },
	{ (Il2CppRGCTXDataType)3, 12456 },
	{ (Il2CppRGCTXDataType)2, 16702 },
	{ (Il2CppRGCTXDataType)3, 12457 },
	{ (Il2CppRGCTXDataType)3, 12458 },
	{ (Il2CppRGCTXDataType)3, 12459 },
	{ (Il2CppRGCTXDataType)2, 13110 },
	{ (Il2CppRGCTXDataType)3, 12460 },
	{ (Il2CppRGCTXDataType)3, 12461 },
	{ (Il2CppRGCTXDataType)2, 13115 },
	{ (Il2CppRGCTXDataType)3, 12462 },
	{ (Il2CppRGCTXDataType)1, 16703 },
	{ (Il2CppRGCTXDataType)2, 13114 },
	{ (Il2CppRGCTXDataType)3, 12463 },
	{ (Il2CppRGCTXDataType)1, 13114 },
	{ (Il2CppRGCTXDataType)1, 13110 },
	{ (Il2CppRGCTXDataType)2, 16702 },
	{ (Il2CppRGCTXDataType)2, 13114 },
	{ (Il2CppRGCTXDataType)2, 13112 },
	{ (Il2CppRGCTXDataType)2, 13116 },
	{ (Il2CppRGCTXDataType)3, 12464 },
	{ (Il2CppRGCTXDataType)3, 12465 },
	{ (Il2CppRGCTXDataType)3, 12466 },
	{ (Il2CppRGCTXDataType)2, 13111 },
	{ (Il2CppRGCTXDataType)3, 12467 },
	{ (Il2CppRGCTXDataType)2, 13126 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	140,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	45,
	s_rgctxIndices,
	220,
	s_rgctxValues,
	NULL,
};
